function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = dssv.length - 1; i >= 0; i--) {
    var item = dssv[i];
    var contentTr = `
      <tr>
          <td> ${item.ma} </td>
          <td> ${item.ten} </td>
          <td> ${item.email} </td>
          <td> ${item.tinhTDB()} </td>
          <td>
          <button onclick="xoaSV('${item.ma}')" class="btn btn-danger">
          Xóa
          </button>
          <button onclick="suaSV('${item.ma}')" class="btn btn-danger">
          Sửa
          </button>
          </td>
      </tr>
       `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;
  return new SinhVien(ma, ten, email, matKhau, toan, ly, hoa);
}
