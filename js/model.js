function SinhVien(ma, ten, email, matKhau, toan, ly, hoa) {
  this.ma = ma;
  this.ten = ten;
  this.email = email;
  this.matkhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhTDB = function () {
    return (this.toan + this.ly + this.hoa) / 3;
  };
}
