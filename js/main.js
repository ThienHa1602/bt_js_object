var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";
var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  dssv = JSON.parse(jsonData).map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  renderDSSV(dssv);
}
function themSV() {
  var sv = layThongTinTuForm();
  dssv.push(sv);
  var dataJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV_LOCAL, dataJson);
  renderDSSV(dssv);
  resetForm();
}
function xoaSV(id) {
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
}
function suaSV(id) {
  console.log("id: ", id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  var sv = dssv[viTri];
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function capNhatSV() {
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dssv[viTri] = sv;
  renderDSSV(dssv);
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
